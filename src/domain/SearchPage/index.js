import React from 'react'

import { Container, Line } from './styles'
import Root from './../../components/atoms/root'
import InputGuia from './../../components/atoms/input'
import InfoHeader from '../../components/molecules/infoheader'

const SearchPage = () => (
  <Container>
    <Line>
      <Root>
        <InfoHeader title="Buscar Contatos" subTitle="Realize a busca de clientes em sua base de dados" />
      </Root>
    </Line>
    <Root>
      <InputGuia placeholder="Pesquisar por nome, telefone ou endereço" type="text" />
    </Root>
  </Container>
)

export default SearchPage
