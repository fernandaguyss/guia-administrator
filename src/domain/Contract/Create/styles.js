import styled from 'styled-components'

export const Container = styled.div``
export const Line = styled.div`
border-bottom: 1px solid var(--gray-border)
`
export const Box = styled.div`
display: flex;
flex-direction: row;
margin-top: 40px;
position: relative;
`
export const Form = styled.form`
display: flex;
flex: 3;
margin-left: 2%;
display: flex;
flex-direction: column;
`
export const FormGroup = styled.div`
flex: 1;
margin-bottom: 4%;
`
export const FormFieldBox = styled.div`
border: 1px solid var(--gray-border);
background-color: rgba(228,228,228, 0.3);
border-radius: 8px;
padding: 24px;
display: flex;
flex: 1;
flex-direction: column;
`

export const Row = styled.div`
display: flex;
flex-direction: row;
`
export const FormActions = styled.div`
display: flex;
justify-content: space-between;
margin: 2% 0 10% 0;
`
export const DefaultAction = styled.div`
display: flex;
flex-direction: row;
`
export const CustomAction = styled.div`
display: flex;
flex-direction: row;
`