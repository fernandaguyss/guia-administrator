import React, { useEffect } from 'react'

import { Container, Line, Box, Form, FormGroup, FormFieldBox, Row, FormActions, DefaultAction, CustomAction } from './styles'
import Root from './../../../components/atoms/root'
import InfoHeader from './../../../components/molecules/infoheader'

import Sidebar from './../../../components/molecules/sidebar'
import InputGuia from './../../../components/atoms/input'
import ButtonGuia from './../../../components/atoms/button'

import FormDescription from './../../../components/atoms/form-description'

const sidebarList = [
  { name: 'Dados do cliente', path: '#data-client' },
  { name: 'Endereço', path: '#address' }
]

const CreateContact = () => {

  const onScroll = (e) => {
    console.log('scroll')
  }

  // useEffect(() => {
  //   window.addEventListener('scroll', onScroll);
  // }, []);

  return (
    <Container>
      <Line>
        <Root>
          <InfoHeader title="Cadastro de novos Contatos" subTitle="Realize cadastro de novos clientes em sua base de dados" />
        </Root>
      </Line>
      <Root>
        <Box>
          <Sidebar list={sidebarList} />
          <Form>
            <FormGroup id="data-client">
              <FormDescription title="Dados do Cliente" subtile="Utilize os dados do cliente para iniciar o cadastro" />
              <FormFieldBox>
                <Row>
                  <InputGuia placeholder="Nome completo" type="text" label="Nome completo" />
                  <InputGuia placeholder="Ex: 47 9 99999999" type="text" label="Telefone" />
                </Row>
                <Row>
                  <InputGuia label="E-mail" placeholder="@guiaschnell.com" type="e-mail" />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormGroup id="address">
              <FormDescription title="Endereço" subtile="Insira os dados do endereço do cliente" />
              <FormFieldBox>
                <Row>
                  <InputGuia label="Logradouro" placeholder="Ex: Rua, Avenida " type="text" />
                  <InputGuia label="Número" placeholder="Ex: 40" type="text" />
                </Row>
                <Row>
                  <InputGuia label="Bairro" placeholder="Nome do Bairro" type="text" />
                  <InputGuia label="CEP" placeholder="Ex: 79402-000" type="text" />
                </Row>
                <Row>
                  <InputGuia label="Cidade" placeholder="Nome da Cidade" type="text" />
                  <InputGuia label="Estado" placeholder="Sigla Estado" type="text" />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormActions>
              <DefaultAction>
                <ButtonGuia text="Voltar" tertiary />
                <ButtonGuia text="Salvar" primary />
              </DefaultAction>
              <CustomAction>
                <ButtonGuia text="Salvar e Torna Cliente" primary />
              </CustomAction>
            </FormActions>

          </Form>
        </Box>
      </Root>
    </Container>
  )
}

export default CreateContact
