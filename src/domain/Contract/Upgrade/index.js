import React, { useState, useEffect } from 'react';

import {
  Container,
  Line,
  Box,
  Form,
  FormGroup,
  FormFieldBox,
  Row,
  FormActions,
  DefaultAction,
  Label,
} from './styles';
import Root from '../../../components/atoms/root';
import InfoHeader from '../../../components/molecules/infoheader';

import Sidebar from '../../../components/molecules/sidebar';
import InputGuia from '../../../components/atoms/input';
import RadioButton from '../../../components/atoms/radio-button';
import ButtonGuia from '../../../components/atoms/button';
import Signature from '../../../components/atoms/signature';

import FormDescription from '../../../components/atoms/form-description';
import Modal from '../../../components/molecules/modal';

const sidebarList = [
  { name: 'Dados do cliente', path: '#data-client' },
  { name: 'Endereço', path: '#address' },
  { name: 'Dados Personalizados e Divulgação', path: '#custom' },
  { name: 'Dados da Venda e Pagamento', path: '#sale' },
  { name: 'Assinatura Digital', path: '#signature' },
];

const CreateContact = () => {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [fixedMenu, setFixedMenu] = useState('fixedMenu');

  const toggleModal = () => {
    setIsOpen(true);
  };

  const onScroll = () => {
    if (window.screenY > 292) {
      setFixedMenu('fixedMenu');
    } else {
      setFixedMenu('');
    }
  };
  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  });

  return (
    <Container>
      <Line>
        <Root>
          <InfoHeader
            title="Tornar um novo contato em Cliente"
            subTitle="Realize cadastro de novos clientes em sua base de dados"
          />
        </Root>
      </Line>
      <Root>
        <Box>
          <Sidebar list={sidebarList} className={fixedMenu} />
          <Form action="#">
            <FormGroup id="data-client">
              <FormDescription
                title="Dados do Cliente"
                subtile="Utilize os dados do cliente para iniciar o cadastro"
              />
              <FormFieldBox>
                <Row>
                  <InputGuia
                    placeholder="Nome completo"
                    type="text"
                    label="Nome completo"
                  />
                  <InputGuia
                    placeholder="Ex: 47 9 99999999"
                    type="text"
                    label="Telefone"
                  />
                </Row>
                <Row>
                  <InputGuia
                    label="E-mail"
                    placeholder="@guiaschnell.com"
                    type="e-mail"
                  />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormGroup id="address">
              <FormDescription
                title="Endereço"
                subtile="Insira os dados do endereço do cliente"
              />
              <FormFieldBox>
                <Row>
                  <InputGuia
                    label="Logradouro"
                    placeholder="Ex: Rua, Avenida "
                    type="text"
                  />
                  <InputGuia
                    label="Número"
                    placeholder="Ex: 40"
                    type="text"
                  />
                </Row>
                <Row>
                  <InputGuia
                    label="Bairro"
                    placeholder="Nome do Bairro"
                    type="text"
                  />
                  <InputGuia
                    label="CEP"
                    placeholder="Ex: 79402-000"
                    type="text"
                  />
                </Row>
                <Row>
                  <InputGuia
                    label="Cidade"
                    placeholder="Nome da Cidade"
                    type="text"
                  />
                  <InputGuia
                    label="Estado"
                    placeholder="Sigla Estado"
                    type="text"
                  />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormGroup id="custom">
              <FormDescription
                title="Dados Personalizados e Divulgação"
                subtile="Esses dados serão usados para que o usuário possa entrar em contato"
              />
              <FormFieldBox>
                <Row>
                  <InputGuia
                    placeholder="Ex: 47 9 99999999"
                    type="text"
                    label="WhatsApp"
                  />
                  <InputGuia
                    placeholder=""
                    type="text"
                    label="Palavra Chave"
                  />
                </Row>
                <Row>
                  <InputGuia
                    label="Texto Complementar"
                    placeholder=""
                    type="textarea"
                  />
                </Row>
                <Row>
                  <Label>Tipos de Divulgação</Label>
                </Row>
                <Row>
                  <RadioButton
                    id="centro"
                    value="centro"
                    name="tipos_divulgacao"
                    text="Centro"
                  />
                  <RadioButton
                    id="gif"
                    value="gif"
                    name="tipos_divulgacao"
                    text="GIF"
                  />
                  <RadioButton
                    id="banner"
                    value="banner"
                    name="tipos_divulgacao"
                    text="Banner"
                  />
                </Row>
                <Row>
                  <RadioButton
                    id="linha"
                    value="linha"
                    name="tipos_divulgacao"
                    text="Linha em Destaque"
                  />
                  <RadioButton
                    id="cartao"
                    value="cartao"
                    name="tipos_divulgacao"
                    text="Cartão"
                  />
                  <RadioButton
                    id="palavra_chave"
                    value="palavra_chave"
                    name="tipos_divulgacao"
                    text="Palavras Chave"
                  />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormGroup id="sale">
              <FormDescription
                title="Dados da Venda e Pagamento"
                subtile="Preencher com os dados para pagamento e conclusão da venda"
              />
              <FormFieldBox>
                <Row>
                  <InputGuia
                    placeholder="CNPJ/CPF"
                    type="text"
                    label="Dados do Cliente"
                  />
                  <InputGuia
                    placeholder="Nome que consta no documento"
                    type="text"
                    label="Nome do Responsável"
                  />
                </Row>
                <Row>
                  <InputGuia
                    placeholder="Ex: 01/06/1993"
                    type="text"
                    label="Data de Nascimento"
                  />
                  <InputGuia
                    placeholder="R$ 200,00"
                    type="text"
                    label="Valor (Reais)"
                  />
                </Row>
                <Row>
                  <Label>Formas de Pagamento</Label>
                </Row>
                <Row>
                  <RadioButton
                    id="boleto"
                    value="boleto"
                    name="formas_de_pagamento"
                    text="Boleto"
                  />
                  <RadioButton
                    id="cartao_credito"
                    value="cartao_credito"
                    name="formas_de_pagamento"
                    text="Cartão de Crédito"
                  />
                  <RadioButton
                    id="cheque"
                    value="cheque"
                    name="formas_de_pagamento"
                    text="Cheque"
                  />
                  <RadioButton
                    id="pix"
                    value="pix"
                    name="formas_de_pagamento"
                    text="Pix"
                  />
                </Row>
                <Row>
                  <InputGuia
                    label="Observações"
                    placeholder=""
                    type="textarea"
                  />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormGroup id="signature">
              <FormDescription
                title="Assinatura Digital"
                subtile="Preencher com a assinatura do cliente concordando com os termos."
              />
              <FormFieldBox>
                <Row>
                  <Signature />
                </Row>
              </FormFieldBox>
            </FormGroup>

            <FormActions>
              <DefaultAction>
                <ButtonGuia text="Cancelar" tertiary />
                <ButtonGuia
                  text="Gerar PDF"
                  primary
                  onClick={() => toggleModal()}
                />
              </DefaultAction>
            </FormActions>
          </Form>
        </Box>
      </Root>
      <Modal modalIsOpen={modalIsOpen} setModalIsOpen={setIsOpen} />
    </Container>
  );
};

export default CreateContact;
