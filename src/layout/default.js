import React from 'react'

import Header from '../components/molecules/header'
import Root from '../components/atoms/root'
import MenuNav from '../components/molecules/menu'

import { Container, BoxHeader, BoxSubMenu } from './styles'

const Layout = ({ children }) => (
  <Container>
    <BoxHeader>
      <Root>
        <Header />
      </Root>
    </BoxHeader>
    <BoxSubMenu>
      <Root>
        <MenuNav />
      </Root>
    </BoxSubMenu>
    {children}
  </Container>
)

export default Layout
