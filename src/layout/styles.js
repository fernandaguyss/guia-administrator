import styled from 'styled-components'

export const Container = styled.div`
  background-color: var(--white);
`
export const BoxHeader = styled.div`
  background-color: var(--white);
`
export const BoxSubMenu = styled.div`
  height: 34px;
  background-color: var(--white);
`
