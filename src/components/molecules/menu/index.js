import React from 'react'

import { Container, MenuLink, Title } from './styles'

const Sidebar = () => (
  <Container>
    <MenuLink to="/search-page">
      <Title>Buscar Contatos</Title>
    </MenuLink>
    <MenuLink to="/contract/create#data-client">
      <Title>Cadastro de Contatos</Title>
    </MenuLink>
  </Container>
)

export default Sidebar
