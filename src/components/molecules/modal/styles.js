import styled from 'styled-components'

import { FiX } from 'react-icons/fi'

import Modal from 'react-modal';

export const Container = styled(Modal)``
export const CloseIcon = styled(FiX)``
export const CloseButton = styled.div`
  display: flex;
  background: blue;
`

export const Box = styled.div`
background: rgba(0,0,0,0.3);
display: flex;
flex: 1;
flex-direction: column;
align-items: center;
`
export const Header = styled.div`
display: flex;
flex:1;
align-items: center;
background: red;
`
export const Content = styled.div`
align-items: center;
flex: 1;
justify-content: center;
display: flex;
flex-direction: column;
`
export const Image = styled.img`

`
export const Title = styled.div`
display: flex;
flex: 1;
flex-direction: column;
align-items: center;
color: rgba(0,0,0, 1);
font-size: 1.4em;
font-weight: bold;
`
export const SubTitle = styled.div``
export const Actions = styled.div`
display: flex;
flex: 1;
flex-direction: column;
width: 100%;
padding: 0 34px;
margin-top: 74px;`
export const Default = styled.div`
margin-top: 10px;
display: flex;
flex: 1;
flex-direction: end;
width: 20%;`
export const Custom = styled.div``