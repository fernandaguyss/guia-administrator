import React from 'react'
import ButtonGuia from '../../atoms/button'

import { Container, CloseIcon, CloseButton, Box, Header, Content, Image, Title, SubTitle, Actions, Default, Custom } from './styles'
import PDFSource from './../../../assets/images/download-pdf.svg'

const Modal = ({ modalIsOpen, setModalIsOpen }) => {
  return (
    <Container isOpen={modalIsOpen}>
      <Box>
        <Header>
          <CloseButton onClick={() => setModalIsOpen(false)}>
            <CloseIcon />
          </CloseButton>
        </Header>
        <Content>
          <Image src={PDFSource} />
          <Title>Contrato finalizado</Title>
          <SubTitle>
            Utilize os botões abaixo para fazer o download do arquivo.
          </SubTitle>
        </Content>
        <Actions>
          <Default>
            <ButtonGuia tertiary text="Sair" />
          </Default>
          <Custom>
            <ButtonGuia primary text="Baixar PDF" />
            <ButtonGuia primary text="Enviar para o WhatsApp" />
          </Custom>
        </Actions>
      </Box>
    </Container>
  );
}

export default Modal