import styled from 'styled-components'

export const Container = styled.header`
  display: flex;
  display-direction: row;
  padding: 16px 0 0 0;
  align-items: center;
  justify-content: space-between;
`
export const Image = styled.img`
  width: 160px;
  left: 0;
  position: relative;
`
export const Profile = styled.div`
  display: flex;
  justify-content: space-between;
  box-shadow: 0 3px 6px rgba(0,0,0,0.3);
  border-radius: 8px;
  border: 1px solid var(--gray-border);
`
export const ProfileActions = styled.div`
  display: flex;
  align-items: center;
`
