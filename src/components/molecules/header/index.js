import React from 'react'

import LogoImage from './../../../assets/images/guia-logotipo.svg'

import { Container, Image, Profile, ProfileActions } from './styles'
import GuiaButton from './../../atoms/button'

const Login = () => (
  <Container>
    <Image src={LogoImage} />
    <Profile>
      <ProfileActions>
        <GuiaButton primary link text="Olá Cleomar" />
        <GuiaButton link text="sair" />
      </ProfileActions>
    </Profile>
  </Container>
)

export default Login
