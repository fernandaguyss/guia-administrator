import styled from 'styled-components'
import { NavHashLink } from 'react-router-hash-link';

export const Container = styled.div`
  display: flex;
  flex: 1;
`;

export const List = styled.ul`
  list-style: none;
`;
export const Item = styled.li`
  padding: 6px 0;
  max-width: 200px;
`
export const Link = styled(NavHashLink)`
  font-size: 15px;
  color: var(--font-base);
  text-decoration: none;
`