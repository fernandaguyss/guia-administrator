import React from 'react'

import { Container, List, Item, Link } from './styles'

const Sidebar = ({ list }) => (
  <Container>
    <List>
      {list && list.length > 0 ? (
        list.map((item) => (
          <Item>
            <Link
              to={`${item.path}`}
              activeStyle={{ fontWeight: 'bold' }}
              smooth
            >
              {item.name}
            </Link>
          </Item>
        ))
      ) : (
        <Item>Sem menu</Item>
      )}
    </List>
  </Container>
);

export default Sidebar;