import React from 'react'

import { Container, Title, SubTitle } from './styles'

const InfoHeader = ({ title, subTitle }) => (
  <Container>
    <Title>{title}</Title>
    <SubTitle>{subTitle}</SubTitle>
  </Container>
)

export default InfoHeader
