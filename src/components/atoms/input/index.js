import React from 'react'

import { Container, Input, Label, TextArea } from './styles'

const InputGuia = ({ placeholder, type, label, error, login }) => (
  <Container>
    {label && <Label>{label}</Label>}
    {type == 'textarea' ? (
      <TextArea rows="4" cols="50"></TextArea>
    ) : (
      <Input
        type={type}
        placeholder={placeholder}
        login={login}
        error={error}
      />
    )}

  </Container>
)

export default InputGuia
