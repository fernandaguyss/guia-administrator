import React, { Component } from 'react'

import SignatureCanvas from 'react-signature-canvas'

import { Container } from './styles'

import GuiaButton from './../button'

class Signature extends Component {
  sigPad = {}

  clear = () => this.sigPad.clear()

  render() {
    return (
      <Container>
        <SignatureCanvas penColor='black'
          canvasProps={{ width: 650, height: 200, className: 'sigCanvas' }}
          backgroundColor="#fff"
          ref={(ref) => { this.sigPad = ref }}
        />
        <GuiaButton text="Limpar assinatura" tertiary onClick={this.clear} />
      </Container >
    )
  }
}


export default Signature;