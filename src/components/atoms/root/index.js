import React from 'react'

import { Container } from './styles'

const Root = ({ children }) => <Container>{children}</Container>

export default Root
