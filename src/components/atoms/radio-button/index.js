import React from 'react'

import { Container, Input, Label } from './styles'

const RadioButton = ({ id, value, name, text, checked }) => (
  <Container>
    <Input type="radio" id={id} name={name} value={value} checked={checked} />
    <Label for={id}>{text}</Label>
  </Container>
)

export default RadioButton
