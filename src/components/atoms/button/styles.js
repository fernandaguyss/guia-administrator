import styled, { css } from 'styled-components'
import { FiLoader } from 'react-icons/fi'

export const Button = styled.button`
  vertical-align: baseline;
  outline: 0;
  border: none;
  cursor: pointer;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  height: 40px;
  text-transform: none;
  line-height: 0.9em;
  font-style: normal;
  text-decoration: none;
  -webkit-user-select: none;
  box-shadow: 0 0 0 1px transparent inset, 0 0 0 0 rgba(34, 36, 38, 0.15) inset;
  display: flex;
  transition: opacity 0.2s ease 0s;
  border-radius: 8px;

  &:hover {
    opacity: 0.8;
    transition: all 0.5s ease-out;
  }

  ${(props) =>
    props.tertiary &&
    css`
      background-color: var(--tertiary);
      border: 1px solid var(--gray-border);
      color: var(--gray-base);
      padding: 0.6em 2.5em;
      margin-right: 24px;
    `}

  ${(props) =>
    props.primary &&
    css`
      background-color: var(--primary);
      color: var(--white);
      padding: 0.6em 2.5em;
    `}

  ${(props) =>
    props.link &&
    css`
      background-color: var(--white);
      color: var(--gray-base);
      padding: 0.6em 1.2em;
    `}

  ${(props) =>
    props.primary && props.link &&
    css`
      background-color: var(--white);
      color: var(--primary);
      padding: 0.6em 1.2em;
      font-weight: bold;
    `}
   

  ${(props) =>
    props.secondary &&
    css`
      background-color: var(--secondary);
      color: var(--white);
      padding: 0.6em 2.2em;
      border: none;
    
      &:hover {
        opacity: 0.9;
      }
    `}

    ${(props) =>
    props.large &&
    css`
      height: 60px;
      padding: 16px;
      flex: 1;
      display: flex;
    `}
  
    ${(props) =>
    props.loading &&
    css`
      pointer-events: none;
      cursor: default;
      opacity: 0.6;
    `}
`

export const IconBox = styled.div`
  display: inline;
  padding: 0 8px;
`

export const Loader = styled(FiLoader)`
  opacity: 0.6;
  animation: rotation 2s infinite linear;

  @keyframes rotation {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(359deg);
    }
  }
`
