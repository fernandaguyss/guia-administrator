import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 30px;
`
export const Title = styled.h1`
  font-size: 20px;
  color: var(--font-base)
`
export const SubTitle = styled.span`
  font-size: 14px;
  color: var(--font-base)
  `